package br.com.kenui.apps.activity;

import br.com.kenui.apps.player.media.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class PlayerMediaActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.main);
		super.onCreate(savedInstanceState);
	}
	
	public void escolherOpcao(View view){
    	if(view.getId() == R.id.camera){
    		Intent intent = new Intent(this, CameraActivity.class);
    		startActivity(intent);
    	}
    }

}
