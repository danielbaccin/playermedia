package br.com.kenui.apps.activity;

import br.com.kenui.apps.player.media.R;
import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;

public class PlayerActivity extends Activity{
	
	private MediaPlayer player;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.player);
		super.onCreate(savedInstanceState);
	}
	@Override
	protected void onStop() {
		super.onStop();
		liberarPlayer();
	}
	
	private void liberarPlayer() {
		if(player != null){
			player.release();
		}
	}
	public void executarMusicaArquivo(View v) {
		player = MediaPlayer.create(this, R.raw.cokavibration);
		player.start();
	}
	public void executar(View v) {
		if(!player.isPlaying()) {
			player.start();
		}
	}
	public void pausar(View v) {
		if(player.isPlaying()) {
			player.pause();
		}
	}
	public void parar(View v) {
		if(player.isPlaying()) {
			player.stop();
		}
	}




}
